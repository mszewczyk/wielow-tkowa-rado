#!/usr/bin/env python
# -*- coding: utf-8 -*-

from funkload.FunkLoadTestCase import FunkLoadTestCase
import socket, os, unittest, httpdaemonserver

SERVER_HOST = os.environ.get('194.29.175.240', 31006)


class TestHttpServer(FunkLoadTestCase):
    def test_dialog(self):
        http
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((SERVER_HOST, httpdeamonserver.PORT))
        for i in len(httpdeamonserver.LIST):
            question, answer = httpdeamonserver.LIST[i]
            sock.sendall(question)
            reply = httpdeamonserver.recv_until(sock, ['.', '!'])
            self.assertEqual(reply, answer)
        sock.close()


if __name__ == '__main__':
    unittest.main()